FROM nginx:stable

RUN export DEBIAN_FRONTEND=noninteractive \
    && apt-get update \
    && apt-get install -y --no-install-recommends --autoremove \
        openssh-client \
        procps \
    && rm -rf /var/lib/apt/lists/*

ADD scripts/hosts.sh /usr/bin/hosts.sh
ADD keys/id_rsa /root/.ssh/id_rsa
ADD keys/id_rsa.pub /root/.ssh/id_rsa.pub

RUN chmod -v 700 /root/.ssh \
    && chmod -v 600 /root/.ssh/id_rsa \
    && chmod -v 644 /root/.ssh/id_rsa.pub \
    && chmod +x /usr/bin/hosts.sh

VOLUME ["/var/www"]

CMD "/usr/bin/nginx"
