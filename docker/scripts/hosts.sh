#!/usr/bin/env bash
HOSTS_LINE="172.17.0.1 host.docker.internal"
HOSTS_LINE_EXISTS=$(cat /etc/hosts | grep "host.docker.internal")

if [[ "$HOSTS_LINE_EXISTS" == "" ]]; then
  echo "Adding host.docker.internal host with 172.17.0.1 address"
  echo "$HOSTS_LINE"  >> /etc/hosts
fi

env
