FROM php:8.0-fpm

RUN export DEBIAN_FRONTEND=noninteractive \
    && apt-get update \
    && apt-get install -y --no-install-recommends --autoremove \
        git \
        zip \
        libxml2-dev \
        libpng-dev \
        libjpeg-dev \
        libmagickwand-dev \
        libfreetype6-dev \
        libwebp-dev \
        unzip \
        zlib1g-dev \
        libzip-dev

RUN pecl install mongodb
RUN pecl install apcu
RUN pecl install imagick
RUN pecl install xdebug

RUN docker-php-ext-install \
        bcmath \
        gd \
        gettext \
        opcache

RUN docker-php-ext-install \
        pcntl \
        pdo_mysql \
        sockets

RUN docker-php-ext-install \
        zip \
        intl \
        sysvsem

RUN docker-php-ext-enable \
        apcu \
        mongodb \
        imagick

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer --version=2.1.5

ADD scripts/hosts.sh /usr/bin/hosts.sh

RUN { \
        echo "xdebug.client_port = 9003"; \
        echo "zend_extension=xdebug.so"; \
        echo "xdebug.mode=debug"; \
        echo "xdebug.discover_client_host = no"; \
        echo "xdebug.start_with_request = yes"; \
        echo "xdebug.remote_enable = 1"; \
        echo "xdebug.remote_port = 9003"; \
        echo "xdebug.idekey = 'esvagrant'"; \
        echo "xdebug.client_host = 'gateway.docker.internal'"; \
    } > /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

RUN { \
    echo "env[APP_ENV]=local"; \
    echo "env[PHP_IDE_CONFIG]=\"serverName=backoffice-dev.restorolla.com\""; \
} >> /usr/local/etc/php-fpm.d/www.conf

RUN mv /usr/local/etc/php/php.ini-development /usr/local/etc/php.ini

WORKDIR "/var/www"

VOLUME ["/var/www"]

CMD "/usr/local/bin/php"
