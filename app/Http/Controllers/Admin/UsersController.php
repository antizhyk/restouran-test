<?php

namespace App\Http\Controllers\Admin;

use App\Components\Admin\TableView\TableAction;
use App\Components\Admin\TableView\TableColumn;
use App\Components\Admin\TableView\TableView;
use App\Http\Controllers\Controller;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::all();
        $table = TableView::instance('Users')
            ->setCollection($users)
            ->addColumn(TableColumn::instance('UID')->setField('_id'))
            ->addColumn(TableColumn::instance('Name')->setView('column-user-name'))
            ->addColumn(TableColumn::instance('Organization')->setField('organizationName'))
            ->addAction(TableAction::instance('Profile')
                ->setUrlCallback(function ($model) {
                    return route('admin.user.profile', ['id' => $model->_id]);
                }));

        return $table->view();
    }

    public function profile(Request $request, $id = null)
    {
        if (!$id) {
            $user = Auth::user();
        } else {
            $user = User::find($id);
        }

        if (!$user) {
            abort(404, 'User not found');
        }

        $organizations = Organization::getActive();
        return view('admin.users.profile', compact('user', 'organizations'));
    }

    public function profilePost(Request $request, $id)
    {
        /** @var User $user */
        $user = User::find($id);
        if (!$user) {
            abort(404, 'User not found');
        }

        if ($orgId = $request->post('organizationID')) {
            $user->organizationID = $orgId;
        } else {
            $user->unset('organizationID');
        }

        $user->roles = $request->post('role');
//        $user->posID = $request->post('posID');

        $user->save();

        return redirect(route('admin.users'));
    }
}
