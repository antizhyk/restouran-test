<?php

namespace App\Http\Controllers\Admin;

use App\Components\Admin\Form\Form;
use App\Components\Admin\Form\FormField;
use App\Components\Admin\Form\FormFieldCheckboxSet;
use App\Components\Admin\Form\FormFieldCustom;
use App\Components\Admin\Form\FormFieldFile;
use App\Components\Admin\Form\FormFieldSelect;
use App\Components\Admin\Form\FormFieldTextarea;
use App\Components\Admin\Form\FormGrid;
use App\Components\Admin\Form\FormGroup;
use App\Components\Admin\TableView\TableAction;
use App\Components\Admin\TableView\TableColumn;
use App\Components\Admin\TableView\TableView;
use App\Components\Forms\OrganizationForm;
use App\Helpers\TimeZoneHelper;
use App\Http\Controllers\Controller;
use App\Models\Organization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class OrganizationController extends Controller
{
    public function create()
    {
        return view('admin.organization.create');
    }

    public function list(Request $request)
    {
        $query = Organization::query();

        if ($status = $request->get('status')) {
            $query->where('status', $status);
        }

        if ($searchQuery = $request->get('q')) {
            $query->where('name', 'like', '%' . $searchQuery . '%');
        }


        $organizations = $query->paginate(6)->appends($request->query());
        return view('admin.organization.list', compact('organizations'));
    }

    public function edit($id, Request $request)
    {
        /** @var Organization $model */
        $model = Organization::find($id) ?: new Organization([

        ]);
        $form = OrganizationForm::instance($model);

        if ($request->isMethod('POST')) {
            $form->store($request);
            return redirect(route('admin.organization.edit', ['id' => $model->_id]));
        }

        return view('admin.organization.edit', compact('model', 'form'));
    }

    public function qr($id)
    {
        $model = Organization::find($id);

        return view('admin.organization.qr', compact('model'));
    }

    public function menu($id)
    {
        $model = Organization::find($id);

        return view('admin.organization.menu', compact('model'));
    }

    public function tables($id)
    {
        $model = Organization::find($id);

        return view('admin.organization.tables', compact('model'));
    }

    public function export($id)
    {
        $model = Organization::find($id);

        return view('admin.organization.export', compact('model'));
    }

    public function import($id)
    {
        $model = Organization::find($id);

        return view('admin.organization.import', compact('model'));
    }
}
