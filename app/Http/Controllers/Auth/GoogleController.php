<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use MongoDB\BSON\ObjectId;
use MongoDB\BSON\UTCDateTime;

class GoogleController extends Controller
{

    public function login()
    {
      session()->put('user_url', url()->previous());
        return view('auth.login');
    }

    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    public function callback()
    {
        $googleUser = Socialite::driver('google')->user();

        $user = User::where('services.google.email', $googleUser->getEmail())->first();
        if (!$user) {
            $user = new User();
            $user->createdAt = new UTCDateTime();
            $user->_id = substr((string)(new ObjectId()), -17);
            $user->services = [
                'google' => [
                    'id' => $googleUser->getId(),
                    'email' => $googleUser->getEmail(),
                    'gender' => $googleUser->user['gender'] ?? '',
                    'familyName' => $googleUser->user['family_name'],
                    'givenName' => $googleUser->user['given_name'],
                    'picture' => $googleUser->user['picture'],
                    'verifiedEmail' => $googleUser->user['verified_email']
                ],
            ];
            $user->profile = [
                'name' => $googleUser->getName()
            ];

            $user->save();
        }

        if ($user) {
            Auth::login($user, true);
        }

      if(session()->get('user_url')){
        return redirect(session()->get('user_url'));
      }
      else
      {
        return redirect(route('admin.users'));
      }

    }

    public function logout()
    {
        Auth::logout();
        return redirect(route('admin.login'));
    }
}
