<?php

namespace App\Models\Traits;

trait DocBlockParser
{
    public static function getProps() {
        $ref = new \ReflectionClass(self::class);
        $docBlock = $ref->getDocComment();
        preg_match_all('/\@property.*\$([a-zA-Z0-9_]+)/', $docBlock, $matches);
        return $matches[1] ?? [];
    }
}