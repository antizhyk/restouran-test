<?php

namespace App\Models;

use App\Models\Attributes\OrganizationAttributes;
use App\Models\Traits\DocBlockParser;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

/**
 * @property string $_id
 * @property array $activity
 * @property array $address
 * @property string $created
 * @property string $currency
 * @property string $description
 * @property int $eventSequence
 * @property int $flushCycle
 * @property string $image
 * @property array $languages
 * @property string $name
 * @property string $status
 * @property array $discount
 * @property array $paymentMethods
 * @property string $timezone
 * @property array $schedule24
 */
class Organization extends Model
{
    use HasFactory, OrganizationAttributes, DocBlockParser;

    protected $collection = 'organization';

    public $attributes = [
        'address' => [],
        'discount' => [],
    ];

    const STATUS_ENABLED = "ENABLED";
    const STATUS_DISABLED = "DISABLED";
    const STATUS_HIDDEN = "HIDDEN";

    const STATUS_LIST = [
        self::STATUS_ENABLED => 'Enabled',
        self::STATUS_DISABLED => 'Disabled',
        self::STATUS_HIDDEN => 'Hidden',
    ];

    const CURRENCY_LIST = [
        'EUR' => "€ Euro",
        'USD' => "$ Dollar",
        'RUB' => "₽ Russian Rouble",
        'UAH' => "₴ Ukrainian hryvnia",
        'GEL' => "ლ Georgian Lari",
    ];

    const PAYMENT_METHOD_CASH = "CASH";
    const PAYMENT_METHOD_CARD = "CARD";
    const PAYMENT_METHOD_TFA = "TFA";

    const PAYMENT_METHODS = [
        self::PAYMENT_METHOD_CARD => 'Card',
        self::PAYMENT_METHOD_CASH => 'Cash',
        self::PAYMENT_METHOD_TFA => 'T.F.A.',
    ];


    public static function getActive()
    {
        return self::whereRaw([
            'status' => [
                '$in' => [self::STATUS_ENABLED, self::STATUS_HIDDEN]
            ]
        ])->orderBy('name')->get();
    }
}
