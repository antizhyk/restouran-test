<?php

namespace App\Models;

use App\Models\Attributes\UserAttributes;
use App\Models\Traits\DocBlockParser;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Jenssegers\Mongodb\Auth\User as Authenticatable;

/**
 * @property string $_id
 * @property $createdAt
 * @property array $activity
 * @property int $organizationID
 * @property Organization $organization
 * @property string[] $roles
 * @property string $posID
 */
class User extends Authenticatable
{
    use HasFactory, Notifiable, UserAttributes, DocBlockParser;

    const ROLE_LOGGED = "LOGGED";
    const ROLE_WAITER = "WAITER";
    const ROLE_MANAGER = "MANAGER";


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    ];


    public function organization()
    {
        return $this->hasOne(Organization::class, '_id', 'organizationID');
    }

    public function hasRole($role)
    {
        return in_array($role, $this->roles ?: []);
    }
}
