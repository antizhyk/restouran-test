<?php

namespace App\Models;

use App\Models\Traits\DocBlockParser;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Storage;
use Jenssegers\Mongodb\Eloquent\Model;

/**
 * @property string $_id
 * @property string $extension
 * @property string $filename
 * @property int $size
 * @property int $status
 */
class Upload extends Model
{
    use HasFactory, DocBlockParser;

    protected $collection = 'uploads';

    public function getUrl(): string
    {
        return Storage::disk('uploads')->url($this->_id . '.' . $this->extension);
    }

    public function deleteFile() {
        Storage::disk('uploads')->delete($this->_id . '.' . $this->extension);
        $this->delete();
    }
}
