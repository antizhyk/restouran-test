<?php


namespace App\Models\Attributes;

use App\Helpers\Utils;
use App\Models\Upload;

/**
 * @property string $addressString
 * @property string $geoString
 * @property array $formFieldScheduleData
 */
trait OrganizationAttributes
{
    public function setAddressPhoneAttribute($value)
    {
        $this->address = array_merge($this->address, [
            'phone' => $value
        ]);
    }

    public function setAddressCountryAttribute($value)
    {
        $this->address = array_merge($this->address, [
            'country' => $value
        ]);
    }

    public function setAddressCityAttribute($value)
    {
        $this->address = array_merge($this->address, [
            'city' => $value
        ]);
    }

    public function setAddressStreetAttribute($value)
    {
        $this->address = array_merge($this->address, [
            'street' => $value
        ]);
    }

    public function setAddressPostcodeAttribute($value)
    {
        $this->address = array_merge($this->address, [
            'postcode' => $value
        ]);
    }

    public function setDiscountDefaultAttribute($value)
    {
        $this->discount = array_merge($this->discount, [
            'default' => (int)$value
        ]);
    }

    public function setDiscountFirstOrderAttribute($value)
    {
        $this->discount = array_merge($this->discount, [
            'firstOrder' => (int)$value
        ]);
    }

    public function getAddressStringAttribute()
    {
        $list = [
            $this->address['country'],
            $this->address['postcode'],
            $this->address['city'],
            $this->address['street'],
        ];
        return implode(', ', array_filter($list));
    }

    public function getGeoStringAttribute()
    {
        return implode(', ', $this->address['geo']['coordinates'] ?? []);
    }

    public function getFormFieldScheduleDataAttribute()
    {
        $res = [];
        $dayFrom = '00:00';
        $dayTo = '00:00';
        for ($day = 0; $day <= 6; $day++) {
            $daySchedule = $this->schedule24[$day] ?? null;
            $dayEnabled = isset($daySchedule);
            $dayFrom = $daySchedule && isset($daySchedule[0][0]) ? Utils::secondsToStrTime($daySchedule[0][0]) : $dayFrom;
            $dayTo = $daySchedule && isset($daySchedule[0][1]) ? Utils::secondsToStrTime($daySchedule[0][1]) : $dayTo;
            if ($dayTo == '00:00') {
                $dayTo = '24:00';
            }

            $res[$day] = [
                'enabled' => $dayEnabled,
                'from' => $dayFrom,
                'to' => $dayTo,
            ];
        }
        return $res;
    }

    public function setScheduleAttribute(array $data)
    {
        $res = [];
        foreach ($data as $day => $item) {
            $checked = $item['checked'] ?? false;
            $from = Utils::strTimeToSeconds($item['from'] ?? '00:00');
            $to = Utils::strTimeToSeconds($item['to'] ?? '24:00');
            $res[$day] = $checked ? [[$from, $to]] : null;
        }
        $this->schedule24 = $res;
    }

    public function getUploadLogoUrl(): string
    {
        /** @var Upload $logo */
        if ($this->uploadLogo && $logo = Upload::find($this->uploadLogo)) {
            return $logo->getUrl();
        }
        return '';
    }

    public function getUploadImageUrl(): string
    {
        /** @var Upload $logo */
        if ($this->uploadImage && $logo = Upload::find($this->uploadImage)) {
            return $logo->getUrl();
        }
        return '';
    }

}
