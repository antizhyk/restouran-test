<?php

namespace App\Models\Attributes;

use App\Models\Organization;

/**
 * @property string $organizationName
 * @property string $lastLoginString
 * @property string $initials
 */
trait UserAttributes
{

    public function getOrganizationNameAttribute()
    {
        return $this->organization ? $this->organization->name : null;
    }

    public function getLastLoginStringAttribute()
    {
        $loginTime = $this->activity ? $this->activity['time']->toDateTime() : null;
        $diffFormat = '';
        if ($loginTime) {
            $now = new \DateTime();
            $timeDiff = $loginTime->diff($now);
            if ($now->getTimestamp() - $loginTime->getTimestamp() > 86400) {
                $diffFormat = '%R%a days %Hh %Im';
            } else {
                $diffFormat = '%Hh %Im';
            }
        }

        return $diffFormat ? $timeDiff->format($diffFormat) : '';
    }

    public function getInitialsAttribute(): string
    {
        $words = explode(' ', $this->profile['name'] ?? '', 2);
        $res = '';
        foreach ($words as $word) {
            if (trim($word) !== '') {
                $res .= strtoupper(trim($word)[0]);
            }
        }
        return $res;
    }
}
