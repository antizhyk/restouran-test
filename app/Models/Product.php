<?php

namespace App\Models;

use App\Models\Attributes\OrganizationAttributes;
use App\Models\Traits\DocBlockParser;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;
use MongoDB\BSON\ObjectId;

/**
 * @property string $_id
 * @property string $name
 * @property int $price
 * @property string $externalId
 * @property ObjectId $organizationId
 *
 */
class Product extends Model
{
    use HasFactory, OrganizationAttributes, DocBlockParser;

    protected $collection = 'product';

    const STATUS_ENABLED = "ENABLED";
    const STATUS_DISABLED = "DISABLED";
    const STATUS_HIDDEN = "HIDDEN";

    const STATUS_LIST = [
        self::STATUS_ENABLED => 'Enabled',
        self::STATUS_DISABLED => 'Disabled',
        self::STATUS_HIDDEN => 'Hidden',
    ];

    public static function getActive()
    {
        return self::whereRaw([
            'status' => [
                '$in' => [self::STATUS_ENABLED, self::STATUS_HIDDEN]
            ]
        ])->orderBy('name')->get();
    }
}
