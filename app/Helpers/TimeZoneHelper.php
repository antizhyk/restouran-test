<?php


namespace App\Helpers;


class TimeZoneHelper
{
    private $timeZoneList = [];

    public static function instance()
    {
        return new static();
    }

    public function getList()
    {
        if ($this->timeZoneList) {
            return $this->timeZoneList;
        }
        $timeZoneArray = timezone_identifiers_list(\DateTimeZone::EUROPE | \DateTimeZone::ASIA);
        $utc = new \DateTime('now', new \DateTimeZone('UTC'));
        $timeZoneList = [];
        foreach ($timeZoneArray as $timeZoneName) {
            $timeZone = new \DateTimeZone($timeZoneName);
            $offset = $timeZone->getOffset($utc);
            $offsetstr = ($offset < 0 ? '-' : '+') . gmdate('H:i', abs($offset));
            $timeZoneList[] = ['name' => $timeZoneName, 'offset' => $offset, 'offsetStr' => $offsetstr];
        }
        uasort($timeZoneList, function ($a, $b) {
            if ($a['offset'] == $b['offset']) {
                return strcmp($a['name'], $b['name']);
                return 0;
            }
            return ($a['offset'] < $b['offset']) ? -1 : 1;
        });
        $this->timeZoneList = $timeZoneList;
        return $timeZoneList;
    }

    public function getValue2TitleList(): array
    {
        $timezones = $this->getList();
        $res = [];
        foreach ($timezones as $timezone) {
            $res[$timezone['name']] = "{$timezone['name']} (UTC {$timezone['offsetStr']})";
        }
        return $res;
    }

}
