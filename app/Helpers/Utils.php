<?php

namespace App\Helpers;

class Utils
{

    public static function strTimeToSeconds(string $time): ?int
    {
        preg_match("/(?<hours>[0-9]{1,2}):(?<minutes>[0-9]{2})/", $time, $matches);

        return $matches
            ? ($matches['hours'] * 60 * 60 + $matches['minutes'] * 60)
            : null;
    }

    public static function secondsToStrTime(int $seconds): string
    {
        $minutes = $seconds / 60;

        return sprintf('%02d:%02d', $minutes / 60, $minutes % 60);
    }
}
