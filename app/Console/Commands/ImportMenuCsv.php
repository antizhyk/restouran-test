<?php

namespace App\Console\Commands;

use App\Models\Product;
use Illuminate\Console\Command;
use MongoDB\BSON\ObjectId;

class ImportMenuCsv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:menu-csv {file : Path to CSV file} {orgID : Organization ID}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import menu command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $filePath = $this->argument('file');
        $organizationId = $this->argument('orgID');

        if (!file_exists($filePath)) {
            $this->error('File not found');
            return 0;
        }

        $this->readCsv($filePath, function ($id, $name, $price) use ($organizationId) {
            /** @var Product $product */
            $product = Product::where('externalID', (string)$id)
                ->where('organizationID', new ObjectId($organizationId))
                ->first();

            if ($product &&
                ($product->name != $name || $product->price != $price)) {
                $product->name = $name;
                $product->price = (int)$price;
                $product->save();

                $this->info("{$product->_id} {$product->name} updated");
            }
        });

        return 0;
    }

    private function readCsv(string $filePath, \Closure $param)
    {
        $file = fopen($filePath, 'r');
        fgetcsv($file); // skip headers
        while (($line = fgetcsv($file)) !== FALSE) {
            if (count($line) == 3) {
                list($id, $name, $price) = $line;
                $param($id, $name, $price);
            }
        }
        fclose($file);
    }
}
