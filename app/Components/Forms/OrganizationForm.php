<?php

namespace App\Components\Forms;

use App\Components\Admin\Form\Form;
use App\Components\Admin\Form\FormField;
use App\Components\Admin\Form\FormFieldCheckboxSet;
use App\Components\Admin\Form\FormFieldCustom;
use App\Components\Admin\Form\FormFieldImage;
use App\Components\Admin\Form\FormFieldSelect;
use App\Components\Admin\Form\FormFieldTextarea;
use App\Components\Admin\Form\FormGrid;
use App\Components\Admin\Form\FormGroup;
use App\Helpers\TimeZoneHelper;
use App\Models\Organization;

class OrganizationForm
{
    public static function instance(Organization $model): Form
    {
        $form = Form::instance($model);
        $form
            ->addComponent(
                FormGroup::instance()
                    ->addComponent(FormFieldSelect::instance('Status', 'status', $model->status, Organization::STATUS_LIST))
                    ->addComponent(
                        FormGrid::instance()
                            ->addComponent(FormFieldImage::instance('Logo', 'uploadLogo', $model->getUploadLogoUrl()))
                            ->addComponent(FormFieldImage::instance('Image', 'uploadImage', $model->getUploadImageUrl()))
                    )
            )->addComponent(
                FormGroup::instance('Info')
                    ->addComponent(FormField::instance('Name', 'name', $model->name))
                    ->addComponent(FormFieldTextarea::instance('Description', 'description', $model->description))

            )->addComponent(
                FormGroup::instance('Address')
                    ->addComponent(FormField::instance('Phone', 'addressPhone', $model->address['phone'] ?? ''))
                    ->addComponent(FormField::instance('Country', 'addressCountry', $model->address['country'] ?? ''))
                    ->addComponent(FormField::instance('City', 'addressCity', $model->address['city'] ?? ''))
                    ->addComponent(FormField::instance('Street', 'addressStreet', $model->address['street'] ?? ''))
                    ->addComponent(FormField::instance('Postcode', 'addressPostcode', $model->address['postcode'] ?? ''))
                    ->addComponent(
                        FormField::instance('GPS', 'geoString', $model->geoString ?? '')
                            ->setDescription('Google maps <a href="https://www.google.com/maps/place/' . $model->geoString . '" target="__blank">Show map</a>')
                    )
                    ->addComponent(
                        FormFieldCheckboxSet::instance('Payment method', 'paymentMethods', $model->paymentMethods ?? '')
                            ->setData(Organization::PAYMENT_METHODS)
                    )
                    ->addComponent(FormFieldSelect::instance('Currency', 'currency', $model->currency ?? '')->setData(Organization::PAYMENT_METHODS))
            )->addComponent(
                FormGroup::instance('Discount')
                    ->addComponent(
                        FormGrid::instance()
                            ->addComponent(FormField::instance('Discount %', 'discountDefault', $model->discount['default'] ?? ''))
                            ->addComponent(FormField::instance('Discount first %', 'discountFirstOrder', $model->discount['firstOrder'] ?? ''))
                    )
            )->addComponent(
                FormGroup::instance('Schedule')
                    ->addComponent(FormFieldSelect::instance('Timezone', 'timezone', $model->timezone, TimeZoneHelper::instance()->getValue2TitleList()))
                    ->addComponent(FormFieldCustom::instance('Schedule', 'schedule', $model->formFieldScheduleData)->setView('schedule'))
            );

        return $form;
    }
}
