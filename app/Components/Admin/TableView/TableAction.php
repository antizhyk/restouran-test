<?php

namespace App\Components\Admin\TableView;

use Jenssegers\Mongodb\Eloquent\Model;

class TableAction
{
    public $label = '';

    protected ?\Closure $urlCallback = null;

    public function __construct($label)
    {
        $this->label = $label;
    }

    public static function instance($label): TableAction
    {
        return new static($label);
    }

    /**
     * @param \Closure $urlCallback
     * @return TableAction
     */
    public function setUrlCallback(?\Closure $urlCallback): TableAction
    {
        $this->urlCallback = $urlCallback;
        return $this;
    }

    public function getUrl(Model $model)
    {
        $callback = $this->urlCallback;
        return $callback && is_callable($callback) ? $callback($model) : '';
    }
}
