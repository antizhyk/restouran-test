<?php

namespace App\Components\Admin\TableView;

class TableColumn
{
    const TYPE_TEXT = 0;

    public $type = self::TYPE_TEXT;
    public $label = '';
    public $field = '';
    public $view = '';

    public function __construct($label)
    {
        $this->label = $label;
    }

    public static function instance($label): TableColumn
    {
        return new static($label);
    }

    /**
     * @param mixed $type
     * @return TableColumn
     */
    public function setType($type): TableColumn
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @param string $label
     * @return TableColumn
     */
    public function setLabel(string $label): TableColumn
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @param string $field
     * @return TableColumn
     */
    public function setField(string $field): TableColumn
    {
        $this->field = $field;
        return $this;
    }

    /**
     * @param string $view
     * @return TableColumn
     */
    public function setView(string $view): TableColumn
    {
        $this->view = $view;
        return $this;
    }
}
