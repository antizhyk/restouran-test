<?php

namespace App\Components\Admin\TableView;

use Illuminate\Support\Collection;
use Jenssegers\Mongodb\Eloquent\Model;

class TableView
{
    public $title = 'Table';

    /**
     * @var Collection
     */
    public $collection;

    /**
     * @var TableColumn[]
     */
    public $columns;

    /**
     * @var TableAction[]
     */
    public $actions;

    public function __construct($title = null)
    {
        if ($title) {
            $this->title = $title;
        }
    }

    public static function instance($title = null)
    {
        return new static($title);
    }

    /**
     * @param Collection $collection
     * @return TableView
     */
    public function setCollection(Collection $collection): TableView
    {
        $this->collection = $collection;
        return $this;
    }

    /**
     * @param TableColumn $column
     * @return TableView
     */
    public function addColumn(TableColumn $column): TableView
    {
        $this->columns[] = $column;
        return $this;
    }

    /**
     * @param TableAction $action
     * @return TableView
     */
    public function addAction(TableAction $action): TableView
    {
        $this->actions[] = $action;
        return $this;
    }

    public function getData(TableColumn $column, Model $model)
    {
        if ($column->view) {
            return view('admin.common.table.' . $column->view, ['model' => $model])->render();
        }

        return $model->{$column->field} ?? 'n/a';
    }

    public function view()
    {
        return view('admin.common.table', ['table' => $this]);
    }
}
