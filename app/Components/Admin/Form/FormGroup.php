<?php

namespace App\Components\Admin\Form;

use phpDocumentor\Reflection\Types\Static_;

class FormGroup implements FormComponentInterface
{
    public string $title = '';

    /**
     * @var FormComponentInterface[]
     */
    public array $components = [];

    public function __construct(string $title = '')
    {
        $this->title = $title;
    }

    /**
     * @return static
     */
    public static function instance($title = '')
    {
        return new static($title);
    }

    public function addComponent(FormComponentInterface $component): FormGroup
    {
        $this->components[] = $component;
        return $this;
    }

    public function draw(): string
    {
        return (string)view('admin.common.form.group', ['group' => $this]);
    }
}
