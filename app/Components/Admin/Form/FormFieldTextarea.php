<?php


namespace App\Components\Admin\Form;


class FormFieldTextarea extends FormField
{
    public string $type = self::TYPE_TEXTAREA;
}
