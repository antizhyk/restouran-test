<?php


namespace App\Components\Admin\Form;


class FormField implements FormComponentInterface
{
    const TYPE_INPUT = 'input';

    const TYPE_SELECT = 'select';

    const TYPE_CHECKBOX_SET = 'checkbox-set';

    const TYPE_FILE = 'file';

    const TYPE_FILE_IMAGE = 'file-image';

    const TYPE_TEXTAREA = 'textarea';

    const TYPE_CUSTOM = 'custom';

    public string $type = self::TYPE_INPUT;

    public string $label = '';

    public string $description = '';

    public string $name = '';

    public array $data = [];

    public $value = null;

    public function __construct($label, $name, $value = '', array $data = [])
    {
        $this->label = $label;
        $this->name = $name;
        $this->value = $value;
        $this->data = $data;
    }

    /**
     * @return $this
     */
    public static function instance($label, $name, $value = '', array $data = []): FormField
    {
        return new static($label, $name, $value, $data);
    }

    /**
     * @param string $type
     * @return FormField
     */
    public function setType(string $type): FormField
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @param string $label
     * @return FormField
     */
    public function setLabel(string $label): FormField
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @param array $data
     * @return FormField
     */
    public function setData(array $data): FormField
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @param mixed|null $value
     * @return FormField
     */
    public function setValue($value): FormField
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @param string $name
     * @return FormField
     */
    public function setName(string $name): FormField
    {
        $this->name = $name;
        return $this;
    }

    public function draw(): string
    {
        return (string)view('admin.common.form.field.' . $this->type, ['field' => $this]);
    }

    /**
     * @param string $description
     * @return FormField
     */
    public function setDescription(string $description): FormField
    {
        $this->description = $description;
        return $this;
    }
}
