<?php


namespace App\Components\Admin\Form;


class FormFieldFile extends FormField
{
    public string $type = self::TYPE_FILE;
}
