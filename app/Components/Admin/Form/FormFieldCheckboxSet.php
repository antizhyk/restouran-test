<?php


namespace App\Components\Admin\Form;


class FormFieldCheckboxSet extends FormField
{
    public string $type = self::TYPE_CHECKBOX_SET;
}
