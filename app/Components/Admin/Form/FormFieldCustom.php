<?php


namespace App\Components\Admin\Form;


class FormFieldCustom extends FormField
{
    public string $type = self::TYPE_CUSTOM;

    public string $view = 'custom';

    public function draw(): string
    {
        return (string)view('admin.common.form.custom.' . $this->view, ['field' => $this]);
    }

    /**
     * @param string $view
     * @return FormFieldCustom
     */
    public function setView(string $view): FormFieldCustom
    {
        $this->view = $view;
        return $this;
    }

}
