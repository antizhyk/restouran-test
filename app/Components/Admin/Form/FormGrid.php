<?php

namespace App\Components\Admin\Form;

class FormGrid extends FormGroup implements FormComponentInterface
{
    public string $columnClass = 'col-sm-6';

    public function draw(): string
    {
        return (string)view('admin.common.form.grid', ['grid' => $this]);
    }

    /**
     * @param string $columnClass
     * @return FormGrid
     */
    public function setColumnClass(string $columnClass): FormGrid
    {
        $this->columnClass = $columnClass;
        return $this;
    }
}
