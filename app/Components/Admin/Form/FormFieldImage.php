<?php

namespace App\Components\Admin\Form;

class FormFieldImage extends FormFieldFile
{
    public string $type = self::TYPE_FILE_IMAGE;
}
