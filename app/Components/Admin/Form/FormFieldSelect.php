<?php


namespace App\Components\Admin\Form;


class FormFieldSelect extends FormField
{
    public string $type = self::TYPE_SELECT;
}
