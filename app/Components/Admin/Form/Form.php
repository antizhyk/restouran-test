<?php

namespace App\Components\Admin\Form;

use App\Models\Upload;
use Illuminate\Support\Facades\Storage;
use Jenssegers\Mongodb\Eloquent\Model;

class Form
{
    /**
     * @var FormComponentInterface[]
     */
    public array $components = [];

    /**
     * @var FormField[]
     */
    public array $fields = [];

    private int $counter = 1;

    private ?Model $model = null;

    public function __construct(?Model $model)
    {
        $this->model = $model;
    }

    public static function instance(?Model $model = null): Form
    {
        return new static($model);
    }

    public function addComponent(FormComponentInterface $component): Form
    {
        $this->components[] = $component;
        return $this;
    }

    public function draw(): string
    {
        return (string)view('admin.common.form.form', ['components' => $this->components]);
    }

    public function store(\Illuminate\Http\Request $request, $components = null)
    {
        $isRoot = false;
        if (!$components) {
            $isRoot = true;
            $components = $this->components;
        }

        foreach ($components as $component) {
            if ($component instanceof FormFieldFile) {
                if ($request->hasFile($component->name)) {
                    $oldFileId = $this->model->{$component->name};
                    $file = $request->file($component->name);
                    $upload = new Upload();
                    $upload->extension = $file->getClientOriginalExtension();
                    $upload->filename = $file->getClientOriginalName();
                    $upload->size = $file->getSize();
                    $upload->save();

                    $file->storeAs('', $upload->_id . '.' . $upload->extension, 'uploads');
                    $this->model->{$component->name} = $upload->_id;

                    $upload = Upload::find($oldFileId);
                    if ($upload) {
                        $upload->deleteFile();
                    }

                }
                if (in_array($component->name, $request->post('delete_file', []))) {
                    $fileId = $this->model->{$component->name};
                    $upload = Upload::find($fileId);
                    if ($upload) {
                        $upload->deleteFile();
                        $this->model->unset($component->name);
                    }
                }
            } else if ($component instanceof FormField) {
                $this->model->{$component->name} = $request->post($component->name);
            }
            if (property_exists($component, 'components')) {
                $this->store($request, $component->components);
            }
        }

        if ($isRoot) {
            $this->model->save();
        }
    }
}
