<?php


namespace App\Components\Admin\Form;


interface FormComponentInterface
{
    public function draw(): string;
}
