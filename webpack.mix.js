const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.disableNotifications();

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .sourceMaps();

mix.combine([
        'resources/falcon-v3.2.0/css/theme.css',
        'resources/falcon-v3.2.0/css/user.css',
    ],
    'public/css/falcon.css'
);

mix.combine([
        'resources/falcon-v3.2.0/js/config.js',
        'resources/falcon-v3.2.0/js/theme.js',
    ],
    'public/js/falcon.js'
)

mix.js('resources/js/admin/admin.js', 'public/js')
    .sass('resources/sass/admin/admin.scss', 'public/css')
    .sourceMaps();

mix.copyDirectory('resources/images', 'public/images');
