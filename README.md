# Объяснения выполнения тестового задания

Этот код деменстрирует выполнение данного: задания https://gitlab.com/eatserv/restorolla-backoffice/-/issues/8, суть которого заключалась в реализации запоминания прилоением url адреса и перенаправлением на него после авторизации. 

Мой код прописан в GoogleController где я:

1. В методе __login__ реализовл добавление адресса с которого перешел пользователь в память сессии.
2. И далее в методе __callback__ после авторизации совершаю редирект на этот адресс путем изъятия его из сессии. 


Запуск в первый раз
===================

## Подготовка
Для MacOS
- а включить loopback-алиас для адреса 127.0.0.1:`sudo ifconfig lo0 alias 127.0.0.200 up` 

## Запуск образа:
1. Клонируем этот репозиторий к себе: `git@gitlab.com:eatserv/restorolla-backoffice.git`
1. Устанавливаем Docker (https://docs.docker.com/get-docker/) и Docker-compose (https://docs.docker.com/compose/install/). Минимальная версия - 19.03.
1. Авторизуемся в Docker Registry GitLab: `docker login registry.gitlab.com` под своими логином и паролем. 
Если включена двух-факторная авторизация, то необходимо создать personal access token и использовать как пароль его, 
подробнее здесь: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html
1. Вытягиваем свежие контейнеры через `docker-compose pull` 
1. Добавляем в host файл записи:
   ```
   127.0.0.200 backoffice-dev.restorolla.com
   ```
1. Чтобы собрать проект запускаем: `yarn`
1. Запускаем ждем пока все соберертся `yarn start`
1. Приложение будет доступно по адресу https://backoffice-dev.restorolla.com

# Полезные команды:
* подключиться по ssh `docker-compose exec php-fpm bash`
* обновить дамп моного `docker-compose run mongo update-data`
* посмотреть логи `docker-compose exec php-fpm tail -n5000 -f ./storage/logs/laravel.log`
* запустить в режиме внешей mongoDB базы `yarn start-rr`

# настройка Xdebug для phpStorm:
* In IDE set up Xdebug to listen on 9003
* Go to File | Settings | Languages & Frameworks | PHP | Servers as add server with 
name and host **'backoffice-dev.restorolla.com'** on port 443 an project root mapped to /var/www . 
It is REQUIRED to use exactly this server name as Xdebug will try to connect to IDE using it

# настройка mongo connection для phpStorm:
* View | Tool Windows | Database > add > data source > MongoDB > URL "mongodb://backoffice-dev.restorolla.com:27017/eatserv"
