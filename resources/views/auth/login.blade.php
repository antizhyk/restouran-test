@extends('layouts.simple')

@section('content')
    <div class="container">
        <a href="{{route('admin.login.redirect')}}" class="btn btn-primary">Login via Google</a>
    </div>
@endsection
