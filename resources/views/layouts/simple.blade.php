<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Admin</title>
    <link href="{{mix('css/admin.css')}}" rel="stylesheet">

</head>
<body>
<main role="main">
    @yield('content')
</main>
<script src="{{mix('js/admin.js')}}"></script>
</body>
</html>
