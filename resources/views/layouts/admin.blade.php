<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin</title>
    <link href="{{mix('css/admin.css')}}" rel="stylesheet">
    <link href="{{mix('css/falcon.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
</head>
<body>
<div class="main" id="top">
    <div class="container" data-layout="container">
        @include('admin.navbar-vertical')
        <div class="content">
            @include('admin.navbar')

            @yield('content')

        </div>
    </div>
</div>
<script src="{{asset('/vendors/popper/popper.min.js')}}"></script>
<script src="{{asset('/vendors/bootstrap/bootstrap.min.js')}}"></script>
<script src="{{asset('/vendors/anchorjs/anchor.min.js')}}"></script>
<script src="{{asset('/vendors/is/is.min.js')}}"></script>
<script src="{{asset('/vendors/echarts/echarts.min.js')}}"></script>
<script src="{{asset('/vendors/progressbar/progressbar.min.js')}}"></script>
<script src="{{asset('/vendors/lodash/lodash.min.js')}}"></script>
<script src="https://polyfill.io/v3/polyfill.min.js?features=window.scroll"></script>
<script src="{{asset('vendors/list.js/list.min.js')}}"></script>

<script src="{{mix('js/admin.js')}}"></script>
<script src="{{mix('js/falcon.js')}}"></script>
</body>
</html>
