<?php
/**
 * @var \App\Models\User $model
 */
?>

<div>
    <a href="{{route('admin.user.profile', ['id'=>$model->_id])}}">{{$model->profile['name'] ?? ''}}</a>
</div>
@if ($model->profile['phoneNumber'] ?? false)
    <div>
        <strong>Phone: </strong> {{$model->profile['phoneNumber'] ?? ''}}
    </div>
@endif
@if ($model->activity)
    <div>
        <small>
            Last login: {{$model->lastLoginString}}
        </small>
    </div>
    <div>
        <small>
            Last version: {{$model->activity ? $model->activity['version'] : ''}}
        </small>
    </div>
@endif
