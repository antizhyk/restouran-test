<?php
/**
 * @var \App\Components\Admin\TableView\TableView $table
 */
?>

@extends('layouts.admin')

@section('content')
    <div class="container mt-4">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <h1>{{$table->title}}</h1>
                </div>

            </div>
            <div class="table-responsive scrollbar">
                <table class="table">
                    <tr>
                        @foreach($table->columns as $column)
                            <th>{{$column->label}}</th>
                        @endforeach
                        <th></th>
                    </tr>
                    @foreach($table->collection as $model)
                        <tr>
                            @foreach($table->columns as $column)
                                <td>{!! $table->getData($column, $model) !!}</td>
                            @endforeach
                            <td>
                                @foreach($table->actions as $action)
                                    <a href="{{$action->getUrl($model)}}" class="btn btn-primary">{{$action->label}}</a>
                                @endforeach
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection
