<?php
/**
 * @var \App\Components\Admin\Form\FormField $field
 */
?>

<div class="mb-3">
    <label for="{{$field->name}}">{{$field->label}}</label>
    <div>
        @if ($field->value)
            <div class="mb-3">
                <a href="{{$field->value}}" target="_blank">
                    <img src="{{$field->value}}" height="200">
                </a>
            </div>
            @else
            <div class="mb-3">
                <a href="{{$field->value}}" target="_blank">
                    <img src="{{asset('images/admin/noimage.jpg')}}" height="200">
                </a>
            </div>
        @endif
    </div>
    <div class="mb-3">
        <input class="form-control form-control-sm" id="{{$field->name}}" name="{{$field->name}}" type="file"/>
    </div>
    @if ($field->value)
        <label>
            <input type="checkbox" name="delete_file[]" value="{{ $field->name }}">
            Delete {{$field->label}}
        </label>
    @endif

    @include('admin.common.form.field.field-description')
</div>

