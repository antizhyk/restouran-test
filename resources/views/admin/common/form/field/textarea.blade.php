<?php
/**
 * @var \App\Components\Admin\Form\FormField $field
 */
?>

<div class="mb-3">
    <label for="{{$field->name}}">{{$field->label}}</label>
    <textarea class="form-control" name="description"
              id="{{$field->name}}">{{old($field->name, $field->value)}}</textarea>
    @include('admin.common.form.field.field-description')
</div>
