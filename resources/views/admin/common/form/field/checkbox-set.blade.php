<?php
/**
 * @var \App\Components\Admin\Form\FormField $field
 */
?>

<div class="mb-3">
    <label>{{$field->label}}</label>

    @foreach($field->data as $value => $title)
        <div class="form-check">
            <input class="form-check-input" name="{{$field->name}}[]" type="checkbox" value="{{$value}}"
                   id="{{$field->name}}_{{$value}}" {{is_array($field->value) && in_array($value, $field->value) ? 'checked' : '' }}>
            <label class="form-check-label" for="{{$field->name}}_{{$value}}">
                {{$title}}
            </label>
        </div>
    @endforeach

    @include('admin.common.form.field.field-description')
</div>

