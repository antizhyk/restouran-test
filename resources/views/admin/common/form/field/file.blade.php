<?php
/**
 * @var \App\Components\Admin\Form\FormField $field
 */
?>

<div class="mb-3">
    <label for="{{$field->name}}">{{$field->label}}</label>
    <div>
        @if ($field->value)
            @if ($field instanceof \App\Components\Admin\Form\FormFieldImage)
                <div class="mb-3">
                    <a href="{{$field->value}}" target="_blank">
                        <img src="{{$field->value}}" height="100">
                    </a>
                </div>
            @else
                <label>
                    <input type="checkbox" name="delete_file[]" value="{{ $field->name }}">
                    [delete]
                </label>
                <a href="{{$field->value}}" target="_blank">{{$field->value}}</a>
            @endif
        @endif
    </div>
    <div class="mb-3">
        <input class="form-control form-control-sm" id="{{$field->name}}" name="{{$field->name}}" type="file" />
    </div>
    <label>
        <input type="checkbox" name="delete_file[]" value="{{ $field->name }}">
        [delete]
    </label>

    @include('admin.common.form.field.field-description')
</div>

