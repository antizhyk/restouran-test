<?php
/**
 * @var \App\Components\Admin\Form\FormField $field
 */
?>

<div class="mb-3">
    <label for="{{$field->name}}">{{$field->label}}</label>
    <input type="text" class="form-control" name="{{$field->name}}" id="{{$field->name}}"
           value="{{old($field->name, $field->value ?? '')}}">
    @include('admin.common.form.field.field-description')
</div>

