<?php
/**
 * @var \App\Components\Admin\Form\FormFieldSelect $field
 */
?>

<div class="mb-3">
    <label for="{{$field->name}}">{{$field->label}}</label>
    <select class="form-select" name="{{$field->name}}" id="{{$field->name}}">
        @foreach($field->data as $value => $title)
            <option value="{{$value}}"
                    @if ($value === old($field->name, $field->value)) selected @endif>
                {{$title}}
            </option>
        @endforeach
    </select>
    @include('admin.common.form.field.field-description')
</div>
