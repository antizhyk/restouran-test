<?php
/**
 * @var \App\Components\Admin\Form\FormField $field
 */
?>
@if ($field->description)
    <small class="form-text">
        {!! $field->description !!}
    </small>
@endif
