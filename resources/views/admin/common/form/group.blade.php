<?php
/**
 * @var \App\Components\Admin\Form\FormGroup $group
 */
?>

<div class="card card-body mb-4">
    @if ($group->title)
        <h5 class="card-title">{{$group->title}}</h5>
    @endif

    @foreach ($group->components as $component)
        {!! $component->draw() !!}
    @endforeach
</div>
