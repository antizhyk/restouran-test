<?php
/**
 * @var \App\Components\Admin\Form\FormFieldCustom $field
 */

$days = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
?>
<div class="mt-3"></div>
@foreach ($days as $day => $dayName)
    <div class="row align-items-center mb-4">
        <div class="col-sm-2 col-md-2">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="1"
                       name="{{$field->name}}[{{$day}}][checked]"
                       id="{{$field->name}}_{{$day}}_check" {{$field->value[$day]['enabled'] ? ' checked' : '' }}>
                <label class="form-check-label" for="{{$field->name}}_{{$day}}_check">
                    {{ucfirst($dayName)}}
                </label>
            </div>
        </div>
        <div class="col-md-3">
            <input type="text" class="form-control" name="{{$field->name}}[{{$day}}][from]" value="{{$field->value[$day]['from']}}">
        </div>
        <div class="col-md-3">
            <input type="text" class="form-control" name="{{$field->name}}[{{$day}}][to]" value="{{$field->value[$day]['to']}}">
        </div>
    </div>
@endforeach
