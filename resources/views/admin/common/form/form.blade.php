<?php
/**
 * @var \App\Components\Admin\Form\Form $form
 */
?>

<form method="POST" enctype="multipart/form-data">
    @csrf

    @foreach($form->components as $component)
        {!! $component->draw() !!}
    @endforeach

    <button class="btn btn-primary">Save</button>
</form>
