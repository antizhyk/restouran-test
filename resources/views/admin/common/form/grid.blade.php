<?php
/**
 * @var \App\Components\Admin\Form\FormGrid $grid
 */
?>

<div class="row">
    @foreach ($grid->components as $component)
        <div class="{{$grid->columnClass}}">
            {!! $component->draw() !!}
        </div>
    @endforeach
</div>
