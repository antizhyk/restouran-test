<?php

$navbar = [
    ['Admin', 'admin.users', [
        ['Users', 'admin.users'],
        ['Organizations list', 'admin.organization.list'],
    ]],
    ['Create organization', 'admin.organization.create', []],
    ['QR Print', 'admin.qr', []]
];
?>

<nav class="navbar navbar-light navbar-glass navbar-top navbar-expand-lg" data-move-target="#navbarVerticalNav"
     data-navbar-top="combo">
    <button class="btn navbar-toggler-humburger-icon navbar-toggler me-1 me-sm-3" type="button"
            data-bs-toggle="collapse" data-bs-target="#navbarVerticalCollapse" aria-controls="navbarVerticalCollapse"
            aria-expanded="true" aria-label="Toggle Navigation"><span class="navbar-toggle-icon"><span
                    class="toggle-line"></span></span></button>

    <a class="navbar-brand" href="{{route('admin.index')}}">
        <div class="d-flex align-items-center py-3">
            <img class="me-2" src="{{asset('images/admin/logo.svg')}}" alt="" height="40"/>
        </div>
    </a>

    <div class="collapse navbar-collapse scrollbar" id="navbarStandard">
        <ul class="navbar-nav">
            @foreach($navbar as $index => list($title, $route, $dropdown))
                @if($dropdown)
                    <li class="nav-item dropdown {{request()->routeIs($route.'*') ? 'active' : ''}}">
                        <a class="nav-link dropdown-toggle" href="#" id="dropdown_{{$index}}" data-bs-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            {{$title}}
                        </a>
                        <div class="dropdown-menu py-0" aria-labelledby="dropdown_{{$index}}">
                            <div class="bg-white dark__bg-1000 rounded-3 py-2">
                                @foreach($dropdown as list($title, $route))
                                    <a class="dropdown-item {{request()->routeIs($route) ? 'active' : ''}}"
                                       href="{{route($route)}}">{{$title}}</a>
                                @endforeach
                            </div>
                        </div>
                    </li>
                @else
                    <li class="nav-item">
                        <a class="nav-link {{request()->routeIs($route) ? 'active' : ''}}"
                           href="{{route($route)}}">{{$title}}</a>
                    </li>
                @endif
            @endforeach

            <li class="nav-item"><a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
            </li>
        </ul>
    </div>
    <ul class="navbar-nav navbar-nav-icons ms-auto flex-row align-items-center">
        <li class="nav-item me-2">
            <span class="material-icons text-secondary fs-1">settings</span>
        </li>
        <li class="nav-item me-2">
            <span class="material-icons text-secondary fs-1">shopping_cart</span>
        </li>
        <li class="nav-item">
            <span class="material-icons text-secondary fs-1">notifications</span>
        </li>
        <li class="nav-item dropdown"><a class="nav-link pe-0" id="navbarDropdownUser" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div class="avatar avatar-s">
                    <div class="avatar-name rounded-circle"><span>{{\Illuminate\Support\Facades\Auth::user()->initials}}</span></div>
                </div>
            </a>
            <div class="dropdown-menu dropdown-menu-end py-0" aria-labelledby="navbarDropdownUser">
                <div class="bg-white dark__bg-1000 rounded-2 py-2">
                    <a class="dropdown-item fw-bold text-warning d-flex align-items-center" href="#!">
                        <span class="material-icons fs-1 me-2">star</span>Go Pro
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#!">Set status</a>
                    <a class="dropdown-item" href="#!">Profile &amp; account</a>
                    <a class="dropdown-item" href="#!">Feedback</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#!">Settings</a>
                    <a class="dropdown-item" href="#!">Logout</a>
                </div>
            </div>
        </li>
    </ul>
</nav>