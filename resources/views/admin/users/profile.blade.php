<?php
/**
 * @var \App\Models\User $user
 * @var \App\Models\Organization $organization
 */
?>
@extends('layouts.admin')

@section('content')
    <div class="container">
        <h1 class="mt-4">User profile: {{$user->profile['name'] ?? ''}}</h1>
        <form class="mt-4" method="POST" action="{{route('admin.user.profile', ['id' => $user->id])}}">
            @csrf
            <div class="card card-body">
                <h5 class="card-title">User info</h5>

                <div class="form-group">
                    <label for="formID">ID</label>
                    <input type="text" class="form-control" id="formID" readonly placeholder="{{$user->_id}}">
                </div>
                <div class="form-group">
                    <label for="formName">Profile Name</label>
                    <input type="text" class="form-control" id="formName" readonly
                           placeholder="{{$user->profile['name'] ?? ''}}">
                </div>
            </div>
            <div class="card card-body mt-4">
                <h5 class="card-title">Employee info</h5>

                <div class="form-group">
                    <label for="formOrganization">Organization</label>
                    <select class="form-control" name="organizationID">
                        <option value="0">[Delete]</option>
                        @foreach($organizations as $organization)
                            <option value="{{$organization->_id}}"
                                    @if ($organization->_id == old('organizationID', $user->organizationID)) selected @endif>
                                {{$organization->name}}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="formRoles">Roles</label>
                    <div class="form-check">
                        <input class="form-check-input" name="role[]" type="checkbox"
                               value="{{\App\Models\User::ROLE_MANAGER}}"
                               id="check1" {{$user->hasRole(\App\Models\User::ROLE_MANAGER) ? 'checked' : ''}}>
                        <label class="form-check-label" for="check1">
                            Manager
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" name="role[]" type="checkbox"
                               value="{{\App\Models\User::ROLE_WAITER}}"
                               id="check2" {{$user->hasRole(\App\Models\User::ROLE_WAITER) ? 'checked' : ''}}>
                        <label class="form-check-label" for="check2">
                            Waiter
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="formPosID">Kiosk mode: posID</label>
                    <input type="text" name="posID" class="form-control" id="formPosID"
                           value="{{old('posID', $user->posID)}}">
                </div>
            </div>
            <div class="form-group mt-4">
                <button class="btn btn-primary" type="submit">Save</button>
            </div>
        </form>
    </div>
@endsection
