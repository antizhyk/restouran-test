<?php

$sidebarItems = [
    ['edit', 'Info', 'admin.organization.edit'],
    ['qr_code_2', 'QR Codes', 'admin.organization.qr'],
    ['list', 'Menu', 'admin.organization.menu'],
    ['grid_on', 'Tables', 'admin.organization.tables'],
    ['file_download', 'Export', 'admin.organization.export'],
    ['save_alt', 'Import', 'admin.organization.import'],
];
?>

@section('sidebar')
    <li class="nav-item">
        @if ($id)
            @foreach($sidebarItems as list($icon, $title, $routeName))
                <a class="nav-link {{request()->routeIs($routeName.'*') ? 'active' : ''}}"
                   href="{{route($routeName, ['id' => $id])}}" role="button">
                    <div class="d-flex align-items-center">
                        <div class="nav-link-icon me-1">
                            <span class="material-icons">{{$icon}}</span>
                        </div>
                        <span class="nav-link-text ps-1">{{$title}}</span>
                    </div>
                </a>
            @endforeach
        @endif
    </li>
@endsection
