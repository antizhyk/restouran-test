<?php
/**
 * @var \App\Models\Organization[]|\Illuminate\Support\Collection $organizations
 */
?>
@extends('layouts.admin')

@section('content')
    <div class="container mt-4">
        <div class="card mb-4">
            <div class="card-body">
                <form class="row g-3">
                    <div class="col-auto">
                        <a class="btn btn-primary d-inline-flex"
                           href="{{route('admin.organization.edit', ['id' => 0])}}">
                            <span class="material-icons me-2">add</span>
                            Add new
                        </a>
                    </div>
                    <div class="col-auto">
                        <select class="form-select" name="status" onchange="this.form.submit()">
                            <option value="">Status: All</option>
                            @foreach(\App\Models\Organization::STATUS_LIST as $value => $title)
                                <option value="{{$value}}" {{$value == request('status') ? 'selected':''}}>
                                    Status: {{$title}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-auto ms-auto">
                        <div class="input-group flex-nowrap">
                            <span class="input-group-text" id="addon-wrapping">
                                <span class="material-icons">search</span>
                            </span>
                            <input class="form-control" type="text" placeholder="Name" name="q"
                                   value="{{request('q')}}"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            @foreach($organizations as $idx => $organization)
                <div class="col-lg-6 col-xl-4 mb-4">
                    <div class="card">
                        <div class="card-header position-relative min-vh-25 mb-5">
                            <div class="bg-holder rounded-3 rounded-bottom-0"
                                 style="background: url({{$organization->getUploadImageUrl() ?: asset('images/admin/noimage.jpg')}})"></div>
                            <div class="avatar avatar-4xl avatar-profile">
                                <img class="img-thumbnail shadow-sm"
                                     src="{{$organization->getUploadLogoUrl() ?: asset('images/admin/noimage.jpg')}}"
                                     width="60px" alt="">
                            </div>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title text-truncate">{{$organization->name ?: 'no title'}}</h5>
                            <p class="card-text text-truncate">Some quick example text to build on the card title and
                                make up the bulk of
                                the
                                card's content.</p>
                            <p class="card-text">Status: {{$organization->status}}</p>
                            <div class="btn-group">
                                <button class="btn btn-primary dropdown-toggle btn-sm" type="button"
                                        data-bs-toggle="dropdown" aria-expanded="false">
                                    Action
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item"
                                       href="{{route('admin.organization.edit', ['id' => $organization->_id])}}">Edit
                                        Info</a>
                                    <a class="dropdown-item" href="#">Menu Edit</a>
                                    <a class="dropdown-item" href="#">Menu Import</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        {{ $organizations->links() }}
    </div>
@endsection