<?php

use App\Components\Admin\Form\FormField;

/**
 * @var \App\Models\Organization $model
 * @var \App\Components\Admin\Form\Form $form
 */
?>
@extends('layouts.admin')

@include('admin.organization.sidebar', ['id'=>$model->id])

@section('content')
    <div class="container mt-4">
        <div class="card mb-4">
            <div class="card-body">
                <h4>Info: {{$model->name}}</h4>
            </div>
        </div>
        @include('admin.common.form.form', compact('form'))
    </div>
@endsection
