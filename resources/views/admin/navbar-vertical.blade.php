<?php

$navbar = [
    ['Admin', 'admin.users', [
        ['Users', 'admin.users'],
        ['Organizations list', 'admin.organization.list'],
    ]],
    ['Create organization', 'admin.organization.create', []],
    ['QR Print', 'admin.qr', []]
];
?>

<nav class="navbar navbar-light navbar-vertical navbar-expand-xl">
    <script>
        var navbarStyle = localStorage.getItem("navbarStyle");
        if (navbarStyle && navbarStyle !== 'transparent') {
            document.querySelector('.navbar-vertical').classList.add(`navbar-${navbarStyle}`);
        }
    </script>
    <div class="d-flex align-items-center">
        <div class="toggle-icon-wrapper">
            <button class="btn navbar-toggler-humburger-icon navbar-vertical-toggle" data-bs-toggle="tooltip"
                    data-bs-placement="left" title="" data-bs-original-title="Toggle Navigation"
                    aria-label="Toggle Navigation"><span class="navbar-toggle-icon"><span
                            class="toggle-line"></span></span></button>
        </div>
        <a class="navbar-brand" href="{{route('admin.index')}}">
            <div class="d-flex align-items-center py-3">
                <img class="me-2" src="{{asset('images/admin/logo.svg')}}" alt="" height="40"/>
            </div>
        </a>
    </div>
    <div class="navbar-collapse collapse" id="navbarVerticalCollapse" style="">
        <div class="navbar-vertical-content scrollbar">
            <ul class="navbar-nav flex-column mb-3" id="navbarVerticalNav">
                @hasSection('sidebar')
                    @yield('sidebar')
                @endif
            </ul>
        </div>
    </div>
</nav>