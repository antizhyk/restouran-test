<?php

use App\Http\Controllers\Admin\OrganizationController;
use App\Http\Controllers\Admin\QrController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Auth\GoogleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware([\App\Http\Middleware\AdminAuthMiddleware::class])->group(function () {
    Route::get('/', [UsersController::class, 'profile'])->name('admin.index');

    Route::get('/users', [UsersController::class, 'index'])->name('admin.users');
    Route::get('/users/profile/{id}', [UsersController::class, 'profile'])->name('admin.user.profile');
    Route::post('/users/profile/{id}', [UsersController::class, 'profilePost']);

    Route::get('/organization/create', [OrganizationController::class, 'create'])->name('admin.organization.create');
    Route::get('/organization/list', [OrganizationController::class, 'list'])->name('admin.organization.list');
    Route::any('/organization/edit/{id}', [OrganizationController::class, 'edit'])->name('admin.organization.edit');
    Route::get('/organization/qr/{id}', [OrganizationController::class, 'qr'])->name('admin.organization.qr');
    Route::get('/organization/menu/{id}', [OrganizationController::class, 'menu'])->name('admin.organization.menu');
    Route::get('/organization/tables/{id}', [OrganizationController::class, 'tables'])->name('admin.organization.tables');
    Route::get('/organization/export/{id}', [OrganizationController::class, 'export'])->name('admin.organization.export');
    Route::get('/organization/import/{id}', [OrganizationController::class, 'import'])->name('admin.organization.import');

    Route::get('/qr', [QrController::class, 'print'])->name('admin.qr');

});

Route::get('/login', [GoogleController::class, 'login'])->name('admin.login');
Route::get('/login/google', [GoogleController::class, 'redirectToProvider'])->name('admin.login.redirect');
Route::get('/login/callback', [GoogleController::class, 'callback']);
Route::get('/logout', [GoogleController::class, 'logout']);

