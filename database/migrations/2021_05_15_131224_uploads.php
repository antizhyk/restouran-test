<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Uploads extends Migration
{
    protected $connection = 'mongodb';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)
            ->table('uploads', function (Blueprint $collection) {
                $collection->string('filename');
                $collection->integer('size');
                $collection->string('extension');
                $collection->integer('status')->default(0);
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)
            ->table('uploads', function (Blueprint $collection) {
                $collection->drop();
            });
    }
}
